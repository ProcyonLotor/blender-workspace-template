#!/bin/sh

me="$(realpath "$0")"
repository="$(dirname "$me")/."

# I have no mouth but i must scream
if [ ! -t 0 ]; then
    if command -v x-terminal-emulator >/dev/null 2>&1; then
        x-terminal-emulator -e "$me"
    elif command -v xdg-terminal >/dev/null 2>&1; then
        xdg-terminal "$me"
    else
        echo "Run me from the fucking terminal!" >"${me}.README"
        exit 1
    fi
    exit 0
fi

git clean -dfX --dry-run "${repository}"

printf "Execute [Y,N]?"; read -r yn
if [ "$yn" = "${yn#[Yy]}" ] ;then 
    echo "Aborting cleanup..."
else
    git clean -dfX "${repository}"
    echo "Done."
fi

printf "Press any key to continue..."; read -r _
exit 0
