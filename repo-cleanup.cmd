@ECHO OFF
SETLOCAL EnableDelayedExpansion

CALL :cleanup --dry-run || GOTO die

CHOICE /M "Execute"
IF !ERRORLEVEL! NEQ 1 (
	ECHO Aborting cleanup...
) ELSE (
	CALL :cleanup || GOTO die
	ECHO Done.
)

:end
PAUSE
EXIT /B 0

:die
PAUSE
EXIT /B %ERRORLEVEL%


:cleanup
	git clean -dfX %* "%~dp0." || EXIT /B
	EXIT /B 0
